package com.example.notes

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface PersonDao {

    @Query("SELECT id FROM CONTACTS WHERE name=:name")
    fun getIdByName(name:String):Long

    @Query("SELECT phoneNumber FROM CONTACTS where name=:name")
    fun getPhoneByName(name:String):String

    @Query("Select * from contacts where id=:id")
    fun getPersonById(id:Long):PersonDbEntity

    @Query("SELECT * FROM CONTACTS WHERE name=:name")
    fun getPersonByName(name:String):PersonDbEntity

    @Insert
    fun insert(personDbEntity:PersonDbEntity)

    @Update
    fun update(personDbEntity: PersonDbEntity)

}