package com.example.notes

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "Notes",
)
data class NoteDbEntity(
    @PrimaryKey(autoGenerate = true) val id:Long,
    val name:String,
    var note:String
) {

    fun toNote():Note = Note(
        id = id,
        name = name,
        note = note
    )

}