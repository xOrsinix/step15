package com.example.notes

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    version = 1,
    entities = [
        PersonDbEntity::class,
        NoteDbEntity::class
    ]
)

abstract class AppDatabase:RoomDatabase() {
    abstract fun getPersonDao():PersonDao

    abstract fun getNoteDao():NoteDao
}