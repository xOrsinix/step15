package com.example.notes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.notes.databinding.FragmentContactsBinding
import com.example.notes.databinding.PersonItemBinding

class PersonAdapter(dataModel: DataModel,contactsBinding: FragmentContactsBinding):RecyclerView.Adapter<PersonAdapter.PersonHolder>() {
    val contactsBinding_my = contactsBinding

    val personList = arrayListOf<Person>()

    val dataModel_my = dataModel

    class PersonHolder(item: View):RecyclerView.ViewHolder(item){
        val binding = PersonItemBinding.bind(item)
        fun bind (person:Person,contactsBinding: FragmentContactsBinding,dataModel: DataModel) = with (binding){
            tvNameItem.text = person.name
            tvPhoneItem.text = person.phoneNumber
            LinLay.setOnClickListener {
                dataModel.currentId.value = dataModel.db.value!!.getPersonDao().getIdByName(person.name)
                Navigation.findNavController(contactsBinding.root).navigate(R.id.fromContactsToNotes)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.person_item,parent,false)
        return PersonHolder(view)
    }

    override fun onBindViewHolder(holder: PersonHolder, position: Int) {
        holder.bind(personList[position],contactsBinding_my,dataModel_my)
    }

    override fun getItemCount(): Int {
        return personList.size
    }

    fun setAll(){
        val personDao = dataModel_my.db.value!!.getPersonDao()
        val nameList = dataModel_my.nameList.value
        if (nameList != null) {
            for (i in nameList){
                val p = personDao.getPersonByName(i).toPerson()
                personList.add(p)
                notifyDataSetChanged()
            }
        }
    }
}