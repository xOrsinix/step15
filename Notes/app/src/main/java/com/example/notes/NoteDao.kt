package com.example.notes

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface NoteDao {

    @Query("SELECT name from Notes WHERE id=:id")
    fun getNameById(id:Long):String

    @Query("SELECT * from NOTES where id=:id")
    fun getNoteById(id:Long):NoteDbEntity

    @Query("SELECT note from NOTES where id=:id")
    fun getNoteTextById(id:Long):String

    @Update
    fun update(note:NoteDbEntity)

    @Insert
    fun insert(note: NoteDbEntity)
}