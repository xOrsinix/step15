package com.example.notes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DataModel:ViewModel() {
    val db:MutableLiveData<AppDatabase> by lazy {
        MutableLiveData<AppDatabase>()
    }

    val nameList:MutableLiveData<ArrayList<String>> by lazy {
        MutableLiveData<ArrayList<String>>()
    }

    val currentId:MutableLiveData<Long> by lazy {
        MutableLiveData<Long>()
    }
}