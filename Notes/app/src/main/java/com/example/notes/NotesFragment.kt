package com.example.notes

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.notes.databinding.FragmentNotesBinding

class NotesFragment : Fragment() {
    private val dataModel:DataModel by activityViewModels()
    lateinit var binding: FragmentNotesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentNotesBinding.inflate(inflater)
        val id:Long = dataModel.currentId.value!!
        val note = dataModel.db.value!!.getNoteDao().getNoteById(id)
        binding.tvName.text=note.name
        binding.etText.setText(note.note)
        binding.etText.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                note.note = p0.toString()
            }

        })
        binding.btConfirm.setOnClickListener {
            dataModel.db.value!!.getNoteDao().update(note)
            Toast.makeText(context,"Updated",Toast.LENGTH_SHORT).show()
        }
        return binding.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==android.R.id.home){
            Navigation.findNavController(binding.root).navigate(R.id.fromNotesToContacts)
        }
        return true
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NotesFragment()
            }
}
