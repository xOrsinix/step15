package com.example.notes

data class Person(val id:Long, val name:String, val phoneNumber:String)
