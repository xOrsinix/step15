package com.example.notes

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "Contacts",
    indices = [
        Index("phoneNumber")
    ]
)
data class PersonDbEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val name: String,
    val phoneNumber:String,
) {

    fun toPerson():Person = Person(
        id = id,
        name = name,
        phoneNumber=phoneNumber
    )

    companion object{
        fun fromPair(pair:Pair<String,String>): PersonDbEntity = PersonDbEntity(
            id = 0,
            name = pair.first,
            phoneNumber = pair.second
        )
    }
}