package com.example.notes

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.Menu
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.room.Room
import com.example.notes.databinding.ActivityMainBinding
import java.util.jar.Manifest

private const val PERMISSION_REQUEST = 10

class MainActivity : AppCompatActivity() {
    private val dataModel:DataModel by viewModels()

    lateinit var binding:ActivityMainBinding

    private var permissions = arrayOf(android.Manifest.permission.READ_CONTACTS)

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M) {
            if (checkPermission(binding.root.context, permissions)) {

            } else
                requestPermissions(permissions, PERMISSION_REQUEST)
        }
        val db:AppDatabase by lazy<AppDatabase> {
            Room.databaseBuilder(applicationContext,AppDatabase::class.java,"database.db")
                .allowMainThreadQueries()
                .build()
        }
        dataModel.nameList.value = getContacts(db)
        dataModel.db.value = db
        Log.i("namelist" ,"${dataModel.nameList.value}")
    }

    fun checkPermission(context:Context,permissionArray: Array<String>):Boolean{
        var allSuccess = true
        for (i in permissionArray.indices){
            if (checkCallingOrSelfPermission(permissionArray[i])==PackageManager.PERMISSION_DENIED)
                allSuccess = false
        }
        return allSuccess
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST){
            var allSuccess =true
            for (i in permissions.indices){
                if (grantResults[i] == PackageManager.PERMISSION_DENIED){
                    allSuccess=false
                    var requestAgain = Build.VERSION.SDK_INT>=Build.VERSION_CODES.M &&  shouldShowRequestPermissionRationale(permissions[i])
                    if (requestAgain){

                    }else
                    {
                        Toast.makeText(binding.root.context,"Go to settings and enable the permission",Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    fun getContacts(db:AppDatabase):ArrayList<String> {

        var phoneNumber:String? = null;

        val nameList = ArrayList<String>()

        //Связываемся с контактными данными и берем с них значения id контакта, имени контакта и его номера:
        val CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        val _ID = ContactsContract.Contacts._ID;
        val DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        val HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        val PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        val Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        val NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        val personDao = db.getPersonDao()

        val noteDao = db.getNoteDao()

        //StringBuffer output = new StringBuffer();
        val contentResolver = getContentResolver();
        val cursor = contentResolver.query(CONTENT_URI, null,null, null, null);

        //Запускаем цикл обработчик для каждого контакта:
        if (cursor!!.getCount() > 0) {

            //Если значение имени и номера контакта больше 0 (то есть они существуют) выбираем
            //их значения в приложение привязываем с соответствующие поля "Имя" и "Номер":
            while (cursor.moveToNext()) {
                val contact_id = cursor.getString(cursor.getColumnIndexOrThrow( _ID ));
                val name = cursor.getString(cursor.getColumnIndexOrThrow(DISPLAY_NAME));
                val hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(HAS_PHONE_NUMBER)));

                //Получаем имя:
                if (hasPhoneNumber > 0) {
                    val phoneCursor = contentResolver.query(PhoneCONTENT_URI, null,
                    Phone_CONTACT_ID + " = ?", Array<String>(1) { contact_id }, null);

                    //и соответствующий ему номер:
                    while (phoneCursor!!.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndexOrThrow(NUMBER));
                    }
                }
                if (phoneNumber!=null && name!=null)
                {
                    val p = PersonDbEntity(0,name,phoneNumber)
                    personDao.insert(p)
                    noteDao.insert(NoteDbEntity(0,p.name,""))
                }
                Log.i("CONTACTS"," ${name}  - $phoneNumber")
                if (name!=null)
                    nameList.add(name)
            }
        }
        return nameList
    }

}

