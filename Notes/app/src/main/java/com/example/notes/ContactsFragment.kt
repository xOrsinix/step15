package com.example.notes

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.notes.databinding.ActivityMainBinding
import com.example.notes.databinding.FragmentContactsBinding

class ContactsFragment : Fragment() {
    private val dataModel:DataModel by activityViewModels()
    lateinit var adapter: PersonAdapter
    lateinit var binding:FragmentContactsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentContactsBinding.inflate(inflater)
        adapter = PersonAdapter(dataModel,binding)
        init()
        return binding.root
    }



    private fun init(){
        binding.apply {
            rcView.layoutManager = LinearLayoutManager(context)
            rcView.adapter=adapter
            adapter.setAll()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ContactsFragment()
            }
    }
